const HtmlWebPackPlugin = require("html-webpack-plugin");
const fs = require('fs');
const path = require('path');

// dynamically generate HtmlWebPackPlugin instances for all templates
const templateDir = './src/templates/';
let HtmlWebPackPlugins = [];

fs.readdirSync(templateDir).forEach(file => {
  HtmlWebPackPlugins.push(
    new HtmlWebPackPlugin({
      template: "./src/templates/" + file,
      chunks: ['script', 'style'],
      filename: file,
    })
  );
});

module.exports = {
  entry: {
    style: './src/styles/index.scss',
    script: './src/scripts/index.js'
  },
  output: {
    path: path.join(__dirname, "public"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(gif|png|jpg|svg)$/,
        loader: 'url-loader'
      },
    ]
  },
  plugins: HtmlWebPackPlugins,
};
